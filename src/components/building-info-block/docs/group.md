多选 - 绑定click事件必须用Group包裹使用。

```js
class Example extends React.Component{
  constructor(){
    super()
    this.handleClick = this.handleClick.bind(this)
  }
  handleClick(record) {
    console.log(record)
  }
  render() {
    return (
      <BuildingInfoBlockGroup style={{ display: 'flex' }} onClick={this.handleClick} single={false}>
        <BuildingInfoBlock
          title="中赢国际"
          desc="杭州/滨江/长河街道"
          imageUrl="https://images.creams.io/2018/05/w0f2ctotcfl6ifzxk3scbi858f6ct9bn.jpg!/fw/200"
          theme="light"
          style={{ marginRight: 10 }}
          key="1"
          size="small"
        />
        <BuildingInfoBlock
          title="中大银泰城"
          desc="117.7万"
          imageUrl="https://images.creams.io/2018/05/9snwivxw7hp40kblcti1jdiqgohqzkxh.jpg!/fw/200"
          theme="light"
          style={{ width: 180, marginRight: 10 }}
          key="2"
        />
        <BuildingInfoBlock
          title="aaa"
          desc="北京/北京"
          imageUrl="https://images.creams.io/2018/04/8aiitr2h4fzkpwwu1w1ars0ovibfr7r7.jpg!/fw/200"
          theme="light"
          style={{ marginRight: 10 }}
          key="3"
          size="large"
        />
      </BuildingInfoBlockGroup>
    )
  }
}
;<Example />
```

单选 - 绑定click事件必须用Group包裹使用。

```js
class Example extends React.Component{
  constructor(){
    super()
    this.handleClick = this.handleClick.bind(this)
  }
  handleClick(record) {
    console.log(record)
  }
  render() {
    return (
      <BuildingInfoBlockGroup style={{ display: 'flex' }} onClick={this.handleClick}>
        <BuildingInfoBlock
          title="中赢国际"
          desc="杭州/滨江/长河街道"
          imageUrl="https://images.creams.io/2018/05/w0f2ctotcfl6ifzxk3scbi858f6ct9bn.jpg!/fw/200"
          theme="light"
          style={{ marginRight: 10 }}
          key="12"
        />
        <BuildingInfoBlock
          title="中大银泰城"
          desc="117.7万"
          imageUrl="https://images.creams.io/2018/05/9snwivxw7hp40kblcti1jdiqgohqzkxh.jpg!/fw/200"
          theme="light"
          style={{ width: 180, marginRight: 10 }}
          key="22"
        />
        <BuildingInfoBlock
          title="aaa"
          desc="北京/北京"
          imageUrl="https://images.creams.io/2018/04/8aiitr2h4fzkpwwu1w1ars0ovibfr7r7.jpg!/fw/200"
          theme="light"
          style={{ marginRight: 10 }}
          key="32"
        />
      </BuildingInfoBlockGroup>
    )
  }
}
;<Example />
```