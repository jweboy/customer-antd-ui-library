import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import Icon from '../icon'
import './style/index.css'

const initFunc = () => { }

/**
 * @author jianglei
 * @description 楼宇小部件包裹组件
 * @example ./docs/group.md
 * @class BuildingInfoBlockGroup
 * @extends {Component}
 */
class BuildingInfoBlockGroup extends Component {
  static defaultProps = {
    prefixCls: 'creams-buildingInfoBlock',
    style: {},
    className: '',
    onClick: (evt, record) => { },
    single: true,
  }
  static propTypes = {
    /** 
     * 组件前缀名
     * @ignore
    */
    prefixCls: PropTypes.string,
    /** 外部行内样式覆盖 */
    style: PropTypes.object,
    /** 点击事件回调 */
    onClick: PropTypes.func,
    /** 外部class覆盖 */
    className: PropTypes.string,
    /** 单选或者多选 */
    single: PropTypes.bool,
  }
  constructor() {
    super()
    
    this.state = {
      _toggleMap: new Map(),
      _activeIndex: null,
    }
  }
  getMuiltipleData = (map) => { 
    let ary = []
    for (const [key, value] of map) {
      if (value) { 
        ary.push(key)
      }
    }
    return ary
  }
  // 单选事件
  _setSingleHandler = (index) => { 
    this.setState(({ _toggleMap }) => { 
      for (const [key] of _toggleMap) {
        _toggleMap.set(key, false)
      }
      _toggleMap.set(index, true)
      
      this.props.onClick(index)

      return { _toggleMap }
    })
  }
  // 多选事件
  _setMultipleHandler = (index) => { 
      this.setState(({ _toggleMap }) => { 
        if (_toggleMap.has(index)) { 
          const isActive = _toggleMap.get(index)
          _toggleMap.set(index, !isActive)
        } else {
          _toggleMap.set(index, true)
        }

        const keys = this.getMuiltipleData(_toggleMap)
        this.props.onClick(keys)

        return { _toggleMap, _activeIndex: index }
      })
  }
  _toggleActive = (index, single) => (evt) => { 
    // 事件代理
    const targetNodeName = evt.target.nodeName.toLocaleLowerCase()
    if (targetNodeName === 'img') { 
      evt.stopPropagation()
    }
    
    if (single) { 
      this._setSingleHandler(index)
    } else {
      this._setMultipleHandler(index)
    }
  }
  renderChildren = (props, state) => React.Children.map(props.children, (reactElement, index) => {
    // generate new element
    let _index = index + 1
    const _indexKey = `$${_index}`
    const eventStyle = !!_index ? { cursor: 'pointer' } : {}
    const eventHandler = !!_index ? this._toggleActive(_indexKey, props.single) : initFunc
    const isCover = state._toggleMap.get(_indexKey)
    const imageSize = reactElement.props.size || 'default'

    // current element
    const combineElement = React.cloneElement(reactElement, {
      _index: _indexKey,
      _activeIndex: state._activeIndex,
      prefixCls: props.prefixCls
    })
    // mask cover
    const iconElement = <Icon className={`${props.prefixCls}-icon`} type="confirm" feedback="normal" />
    const maskElement = React.createElement('span', {
      className: isCover ? `${props.prefixCls}-mask-${imageSize}` : `${props.prefixCls}-mask-hidden`
    }, iconElement)
    // event container
    const eventElement = React.createElement('div', {
      className: `${props.prefixCls}-event`,
      style: eventStyle,
      onClick: eventHandler,
    }, [combineElement, maskElement])

    return React.cloneElement(eventElement)
  })
  render() {
    const { prefixCls, style, className } = this.props
    const classes = classnames({
      [`${prefixCls}-group`]: `${prefixCls}-group`
    }, className)
    return (
      <div className={classes} style={style}>
        {this.renderChildren(this.props, this.state)}
      </div>
    )
  }
}

export default BuildingInfoBlockGroup