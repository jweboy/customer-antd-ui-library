import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import IconImage from './icon-image'
import InfoBlock from '../info-block'
import './style/index.css'

/**
 * @author jianglei
 * @description 楼宇小部件
 * @example ./docs/block.md
 * @class BuildingInfoBlock
 * @extends {Component}
 */
class BuildingInfoBlock extends Component {
  static defaultProps = {
    title: '',
    desc: '',
    imageUrl: '',
    size: 'default',
    prefixCls: 'creams-buildingInfoBlock',
    theme: 'default',
    style: {},
    className: '',
    type: 'default'
  }
  static propTypes = {
    /** 标题 */
    title: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.node
    ]),
    /** 描述 */
    desc: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.node
    ]),
    /** 图片地址 */
    imageUrl: PropTypes.string,
    /** 图片大小 */
    size: PropTypes.oneOf(['default', 'small', 'large']),
    /** 
     * 组件前缀名
     * @ignore
    */
    prefixCls: PropTypes.string,
    /** 颜色主题 */
    theme: PropTypes.oneOf(['default', 'light', 'dark']),
    /** 外部行内样式覆盖 */
    style: PropTypes.object,
    /** 外部class覆盖 */
    className: PropTypes.string,
    type: PropTypes.oneOf(['default', 'normal'])
  }
  shouldComponentUpdate(nextProps, nextState) {
    // FIXME: 这步操作在多选情况下只render当前点击的元素,在单选情况下不render(只改变mask状态,无render也没影响)
    if (nextProps._activeIndex !== this.props._index) { 
      return false
    }
    return true
  }
  render() {
    const { title, desc, prefixCls, theme, style, className, size, imageUrl, type, _index } = this.props;
    const classes = classnames({
      [`${prefixCls}`]: prefixCls,
      [`${prefixCls}-${theme}`]: theme,
      [`${prefixCls}-interactive`]: !!_index,
    }, className)
    const styles = {
      border: type === 'default' ? '1px solid #d9d9d9' : 'none',
      ...style
    }
    return (
      <div className={classes} style={styles}>
        <IconImage size={size} imageUrl={imageUrl} prefixCls={prefixCls} />
        <InfoBlock 
          title={<span className={`${prefixCls}-${theme}-title`}>{title}</span>}
          content={<span className={`${prefixCls}-content`}>{desc}</span>}
          className={`${prefixCls}-overwrite`}
        />
      </div>
    )
  }
}

export default BuildingInfoBlock