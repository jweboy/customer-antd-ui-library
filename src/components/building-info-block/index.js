import BuildingInfoBlock from './block'
import BuildingInfoBlockGroup from './BuildingInfoBlockGroup'

BuildingInfoBlock.Group = BuildingInfoBlockGroup

export {
  BuildingInfoBlockGroup
}

export default BuildingInfoBlock