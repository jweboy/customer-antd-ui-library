```js

class Example extends React.Component{
  handleClick(evt) {
    console.log(evt.target)
  }
  render() {
    return [
      <span key="icon1">
        normal:
        <Icon type="wx-s" style={{ fontSize: 24, marginRight: 10 }}  />
      </span>,
      <span key="icon2">
        click:
        <Icon type="wx-s" style={{ fontSize: 24, marginRight: 10, }} onClick={this.handleClick} />
      </span>,
      <span key="icon3">
        disabled:
        <Icon type="wx-s" style={{ fontSize: 24 }}  disabled />
      </span>
    ]
  }
}
;<Example />
```

对照表

```js

const data = [
  // 每一行对应iconfont的每一行图标，如果更新需要相互对应。
  // No.1
  'confirm', 'help-circle-o', 'transform-floorplan','flag-floorplan', 'plus-new', 'up', 'wx-s', 'll-s',
  // No.2
  'dd-s', 'filter', 'plan-floorplan', 'reset-floorplan', 'delete-floorplan', 'eraser-floorplan', 'copy-floorplan', 'enlargement-floorplan',
  // No.3
  'zoom-floorplan', 'point-floorplan', 'shrink-floorplan', 'upload-floorplan', 'redo-floorplan', 'module-new-floorplan', 'expand-floorplan', 'download',
  // No.4
  'mail', 'more', 'data-int', 'custom-int', 'close', 'notice-det', 'remind-det', 'search',
  // No.5
  'print-det', 'enclosure-det', 'setting-int', 'keywords-int', 'password', 'delete-det', 'remarks-det', 'history-det',
  // No.6
  'edit-det', 'return-int', 'modulemanagement-int', 'calendar', 'personnel-det', 'collection-int'
]

class IconGroup extends React.Component {
  renderItem(item, index) {
    return (
      <li key={index} style={{
        display: 'inline-flex',
        alignItems: 'center',
        flexDirection: 'column',
        margin: 20,
      }}>
        <Icon type={item} style={{ fontSize: 24 }} />
        {item}
      </li>
    )
  }
  render() {
    return (
      <ul style={{ background: '#f2f2f2' }}>{data.map(this.renderItem)}</ul>
    )
  }
}
;<IconGroup />
```