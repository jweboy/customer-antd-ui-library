import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import { Button as AntButton } from 'antd'
import './style/index.css'

// TODO: button outline的问题
// FIXME: 目前需求还没有ghost(透明)需求和dashed(虚线)需求,因为还没有定制这个类型的样式,后续有需求再增加。

/**
 * @description Button组件包装
 * @author jianglei
 * @example ./docs/button.md
 * @class Button
 * @extends {Component}
 */
class Button extends AntButton {
  //项目部分定制属性
  static defaultProps = {
    icon: null,
    disabled: false,
    type: 'normal',
    size: 'default',
    onClick: () => { },
    // transparent: false,
    prefixCls: 'creams-btn',
  }
  static propTypes = {
    /** 设置按钮的图标类型 */
    icon: PropTypes.string,
    /** 按钮失效状态 */
    disabled: PropTypes.bool,
    type: PropTypes.oneOf(['normal', 'primary', 'danger', 'gradient', 'secondary']), 
    size: PropTypes.oneOf(['small', 'large', 'default']),
    /** click 事件的 handler */
    onClick: PropTypes.func,
    // transparent: PropTypes.bool,
    /**
     * 组件前缀 
     * @ignore 
    */
    prefixCls: PropTypes.string,
  }
  render() {
    const { type, size, children, prefixCls, disabled, ...otherProps } = this.props
    const classes = classnames({
      [`${prefixCls}-${size}`]: size,
      [`${prefixCls}-${type}`]: type,
      [`${prefixCls}-disabled-${disabled}`]: disabled,
    })
    return (
      <AntButton
        type={type}
        // ghost={transparent}
        className={classes}
        {...otherProps}
      >{children}</AntButton>
    )
  }
}

export default Button