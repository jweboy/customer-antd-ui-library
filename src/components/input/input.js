import React, { Component } from 'react'
import { Input as AntInput } from 'antd'
import './style/index.css'

/**
 * @author jianglei
 * @example ./docs/input.md
 * @class Input
 * @extends {Component}
 */
class Input extends Component {
  render () {
    return (
      <AntInput {...this.props} />
    )
  }
}

export default Input