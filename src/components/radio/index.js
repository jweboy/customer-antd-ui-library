import Radio from './radio'
import RadioGroup from './group'

Radio.Group = RadioGroup

export {
  RadioGroup
}

export default Radio