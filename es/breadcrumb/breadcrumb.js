/*
 * @Author: jweboy 
 * @Date: 2018-06-07 13:49:14 
 * @Last Modified by: jweboy
 * @Last Modified time: 2018-06-12 18:39:54
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Breadcrumb as AntBreadcrumb } from 'antd'
import './style/index.css'

/**
 * @author jianglei
 * @description 面包屑组件包装
 * @example ./docs/breadcrumb.md
 * @class CrBreadcrumb
 * @extends {Component}
 */
class Breadcrumb extends AntBreadcrumb {
  static defaultProps = {
    itemRender: (route, params, routes, paths) => {},
    params: {},
    routes: [],
    separator: '/'
  }
  static propTypes = {
    /** 自定义链接函数，和 react-router 配置使用 */
    itemRender: PropTypes.func,
    /** 路由参数 */
    params: PropTypes.object,
    /** router 的路由栈信息 */
    routes: PropTypes.array,
    /** 分隔符自定义 */
    separator: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.node
    ])
  }
  render() {
    return (
      <AntBreadcrumb {...this.props}>{this.props.children}</AntBreadcrumb>
    )
  }
}

Breadcrumb.Item = AntBreadcrumb.Item

export default Breadcrumb