Radio组合
```js

const RadioGroup = Radio.Group

class RadioGroupExample extends React.Component {
  constructor(){
    super()
    this.state = {
      value: "A",
    }
    this.onChange = this.onChange.bind(this)
  }
  onChange(evt) {
    console.log('radio checked', evt.target.value);
    this.setState({
      value: evt.target.value,
    });
  }
  render() {
    return(
      <RadioGroup onChange={this.onChange} value={this.state.value}>
        <Radio value="A">A</Radio>
        <Radio value="B">B</Radio>
        <Radio value="C">C</Radio>
        <Radio value="D">D</Radio>
      </RadioGroup>
    )
  }
}
;<RadioGroupExample />
```

```js

const RadioButton = Radio.Button

class RadioButtonGroupExample extends React.Component {
  constructor(){
    super()
    this.state = {
      value: "HangZhou",
    }
    this.onChange = this.onChange.bind(this)
  }
  onChange(evt) {
    console.log('radio checked', evt.target.value);
    this.setState({
      value: evt.target.value,
    });
  }
  render() {
    return [
      <RadioGroup 
        onChange={this.onChange} 
        value={this.state.value} 
        key="activce"
        style={{ marginRight: 20 }}
      >
        <RadioButton value="HangZhou">杭州</RadioButton>
        <RadioButton value="ShangHai">上海</RadioButton>
        <RadioButton value="BeiJing">北京</RadioButton>
      </RadioGroup>,
      <RadioGroup 
        theme="dark"
        defaultValue="WuHan" 
        key="dark-theme"
        onChange={this.onChange} 
        value={this.state.value} 
        style={{ marginRight: 20 }}
      >
        <RadioButton value="WuHan">武汉</RadioButton>
        <RadioButton value="ChongQin">重庆</RadioButton>
        <RadioButton value="ChengDu">成都</RadioButton>
      </RadioGroup>,
      <RadioGroup 
        disabled 
        defaultValue="HangZhou" 
        key="disabled"
        style={{ marginRight: 20 }}
      >
        <RadioButton value="HangZhou">杭州</RadioButton>
        <RadioButton value="ShangHai">上海</RadioButton>
        <RadioButton value="BeiJing">北京</RadioButton>
      </RadioGroup>
    ]
  }
}
;<RadioButtonGroupExample />
```