import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import './style/index.css'

/**
 * @author jianglei
 * @description 图标组件,基于iconfont
 * @example ./docs/icon.md
 * @class Icon
 * @extends {Component}
 */
class Icon extends Component {
  static defaultProps = {
    prefixCls: 'iconfont',
    type: '',
    style: {},
    className: '',
    disabled: false,
    onClick: null,
    feedback: 'default'
  }
  static propTypes = {
    /** 
     * 前缀名
     * @ignore
     */
    prefixCls: PropTypes.string,
    /** 图标类型 */
    type: PropTypes.string,
    /** 外部覆盖样式 */
    style: PropTypes.object,
    /** 不可点击状态 */
    disabled: PropTypes.bool,
    /** 点击事件 */
    onClick: PropTypes.oneOfType([
      PropTypes.func,
      PropTypes.instanceOf(null)
    ]),
    /** 外部class覆盖  */
    className: PropTypes.string,
    /** 类型 */
    feedback: PropTypes.oneOf(['default', 'normal'])
  }
  render() {
    const { prefixCls, type, style, disabled, onClick, className, feedback } = this.props;
    const classes = classnames({
      [`${prefixCls}`]: prefixCls,
      [`icon-${type}`]: type,
      [`${prefixCls}-disabled-${disabled}`]: disabled,
      'creams-icon': feedback === 'default'
    }, className)
    const styles = {
      cursor: typeof onClick === 'function' ? 'pointer' : 'initial',
      ...style,
    }
    return (
      <i className={classes} style={styles} onClick={onClick} />
    )
  }
}

export default Icon