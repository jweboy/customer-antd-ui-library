```js

class Example extends React.Component{
  handleClick(evt) {
    console.log(evt.target)
  }
  render() {
    return [
      <span key="icon1">
        normal:
        <Icon type="wx-s" style={{ fontSize: 24, marginRight: 10 }}  />
      </span>,
      <span key="icon2">
        click:
        <Icon type="wx-s" style={{ fontSize: 24, marginRight: 10, }} onClick={this.handleClick} />
      </span>,
      <span key="icon3">
        disabled:
        <Icon type="wx-s" style={{ fontSize: 24 }}  disabled />
      </span>
    ]
  }
}
;<Example />
```