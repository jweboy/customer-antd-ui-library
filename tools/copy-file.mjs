import fsExtra from 'fs-extra'

const rootPath = process.cwd()

try{
  fsExtra.copySync(`${rootPath}/src/components`, `${rootPath}/es`)
} catch(err) {
  process.exit(0)
}
