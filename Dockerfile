FROM nginx:1.12-alpine
WORKDIR /var/www/creams-web
COPY nginx.conf /etc/nginx/
COPY ./dist /var/www/creams-web
CMD ["nginx", "-g", "daemon off;"]
