process.env.NODE_ENV = process.env.NODE_ENV || "development";

// const path = require('path')

const paths = require("react-app-rewired/scripts/utils/paths");
require(paths.scriptVersion + "/config/env");

const webpackConfig = (process.env.NODE_ENV === 'production')
    ? paths.scriptVersion + '/config/webpack.config.prod'
    : paths.scriptVersion + '/config/webpack.config.dev';
    
const config = require(webpackConfig);
const override = require(paths.configOverrides);

const overrideFn =
  typeof override === "function"
    ? override
    : override.webpack || ((config, env) => config);

module.exports = {
  title: 'creams-ui 2.0',
  styleguideDir: 'dist',
  ignore: ['**/index.js', '**/util/*'],
  webpackConfig: overrideFn(config, process.env.NODE_ENV),
  theme: {
    color: {
      link: '#999da1',
      linkHover: '#78a5f3',
      // baseBackground: '#f2f2f2', // 整体背景色
      // codeBackground: '#282a36', // 代码块儿背景色
      // sidebarBackground: '#fff', // 左侧栏背景色
      // ribbonBackground: '#000' // 右上角标fork背景色
    }
  },
  // 定义组件的路径，如：src/components/radio/radio.js
  // getComponentPathLine(componentPath) {
  //   const name = path.basename(componentPath, '.js')
  //   const dir = path.dirname(componentPath)
  //   return null
  // }
}