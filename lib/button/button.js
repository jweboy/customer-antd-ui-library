"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames2 = _interopRequireDefault(require("classnames"));

var _antd = require("antd");

require("./style/index.css");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// TODO: button outline的问题
// FIXME: 目前需求还没有ghost(透明)需求和dashed(虚线)需求,因为还没有定制这个类型的样式,后续有需求再增加。

/**
 * @description Button组件包装
 * @author jianglei
 * @example ./docs/button.md
 * @class Button
 * @extends {Component}
 */
var Button =
/*#__PURE__*/
function (_AntButton) {
  _inherits(Button, _AntButton);

  function Button() {
    _classCallCheck(this, Button);

    return _possibleConstructorReturn(this, _getPrototypeOf(Button).apply(this, arguments));
  }

  _createClass(Button, [{
    key: "render",
    //项目部分定制属性
    value: function render() {
      var _classnames;

      var _this$props = this.props,
          type = _this$props.type,
          size = _this$props.size,
          children = _this$props.children,
          prefixCls = _this$props.prefixCls,
          disabled = _this$props.disabled,
          otherProps = _objectWithoutProperties(_this$props, ["type", "size", "children", "prefixCls", "disabled"]);

      var classes = (0, _classnames2.default)((_classnames = {}, _defineProperty(_classnames, "".concat(prefixCls, "-").concat(size), size), _defineProperty(_classnames, "".concat(prefixCls, "-").concat(type), type), _defineProperty(_classnames, "".concat(prefixCls, "-disabled-").concat(disabled), disabled), _classnames));
      return _react.default.createElement(_antd.Button, _extends({
        type: type // ghost={transparent}
        ,
        className: classes
      }, otherProps), children);
    }
  }]);

  return Button;
}(_antd.Button);

_defineProperty(Button, "defaultProps", {
  icon: null,
  disabled: false,
  type: 'normal',
  size: 'default',
  onClick: function onClick() {},
  // transparent: false,
  prefixCls: 'creams-btn'
});

_defineProperty(Button, "propTypes", {
  /** 设置按钮的图标类型 */
  icon: _propTypes.default.string,

  /** 按钮失效状态 */
  disabled: _propTypes.default.bool,
  type: _propTypes.default.oneOf(['normal', 'primary', 'danger', 'gradient', 'secondary']),
  size: _propTypes.default.oneOf(['small', 'large', 'default']),

  /** click 事件的 handler */
  onClick: _propTypes.default.func,
  // transparent: PropTypes.bool,

  /**
   * 组件前缀 
   * @ignore 
  */
  prefixCls: _propTypes.default.string
});

var _default = Button;
exports.default = _default;