按钮尺寸:

按钮有大(38px)、中(28px)、小(24px)三种尺寸, 这里主要指的是按钮高度。

```js
<Button size="small">24px-small-btn</Button>&emsp;
<Button>28px-default-btn</Button>&emsp;
<Button size="large">38px-large-btn</Button>&emsp;
```

按钮类型:

主按钮、次按钮、危险按钮、渐变按钮、禁用按钮。

```js
<Button type="primary">primary-btn</Button>&emsp;
<Button type="secondary">secondary-btn</Button>&emsp;
<Button type="danger">danger-btn</Button>&emsp;
<Button type="gradient">gradient-btn</Button>&emsp;
<Button disabled>disable-btn</Button>&emsp;
```
