"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _util = require("../util");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var sizeTable = {
  'small': {
    width: 40,
    height: 40
  },
  'default': {
    width: 54,
    height: 36
  },
  'large': {
    width: 90,
    height: 56
  } // FIXME: image加载成功后增加透明度，这个点可续可以优化为默认深色系背景，图片加载成功后高斯模糊并清晰展示。

};

var IconImage =
/*#__PURE__*/
function (_Component) {
  _inherits(IconImage, _Component);

  function IconImage() {
    _classCallCheck(this, IconImage);

    return _possibleConstructorReturn(this, _getPrototypeOf(IconImage).apply(this, arguments));
  }

  _createClass(IconImage, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this = this;

      var _this$props = this.props,
          imageUrl = _this$props.imageUrl,
          prefixCls = _this$props.prefixCls;
      var loaderImage = new Image();
      loaderImage.src = imageUrl;

      loaderImage.onload = function () {
        // TODO: 采用新的styleMap API改进
        _this.imageNode.style.backgroundImage = "url('".concat(imageUrl, "')");

        _this.imageNode.classList.add("".concat(prefixCls, "-image-animate"));
      };
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props2 = this.props,
          prefixCls = _this$props2.prefixCls,
          size = _this$props2.size,
          imageUrl = _this$props2.imageUrl;

      if (!(0, _util.matchUrl)(imageUrl)) {
        // console.warn('无效的image url, 请检查!');
        return null;
      }

      return _react.default.createElement("div", {
        className: "".concat(prefixCls, "-").concat(size, "-image"),
        style: sizeTable[size],
        ref: function ref(imageNode) {
          _this2.imageNode = imageNode;
        }
      });
    }
  }]);

  return IconImage;
}(_react.Component);

var _default = IconImage;
exports.default = _default;