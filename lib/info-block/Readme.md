props-title各类情况

```js
class Example extends React.Component{
  render() {
    return (
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <InfoBlock title="面积" content={127834} />
        <InfoBlock title={<span>尺寸</span>} content="中等" />
        <InfoBlock title="面积" titleChild={<Icon style={{ paddingLeft: 10 }} type="info-circle-o" />} content={127834} />
        <InfoBlock title="在租均价" content={<Input placeholder="请填写在租均价" />} />
      </div>
    )
  }
}
;<Example />
```

props-content各类情况

```js
class Example extends React.Component{
  render() {
    return (
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <InfoBlock title="面积" content={127834} />
        <InfoBlock title="面积" content="区域面积" />
        <InfoBlock title="面积" content={null} />
        <InfoBlock title="面积" content={undefined} />
        <InfoBlock title="面积" content={<p><span>348293</span>,<span>9090</span></p>} />
      </div>
    )
  }
}
;<Example />
```

支持click事件绑定以及children子节点

```js
class Example extends React.Component{
  handleClick(evt) {
    console.log(evt.target)
  }
  render() {
    return (
      <div style={{ display: 'flex', justifyContent: 'space-around' }}>
        <InfoBlock title="面积" content={127834} onClick={this.handleClick}/>
        <InfoBlock title="面积" content={127834}>
          <span>test</span>
        </InfoBlock>
      </div>
    )
  }
}
;<Example />
```

