"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames2 = _interopRequireDefault(require("classnames"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

require('./style/index.css');
/**
 * @author jianglei
 * @description 块组件
 * @example ./docs/block.md
 * @class InfoBlock
 * @extends {Component}
 */


var InfoBlock =
/*#__PURE__*/
function (_Component) {
  _inherits(InfoBlock, _Component);

  function InfoBlock() {
    var _getPrototypeOf2;

    var _temp, _this;

    _classCallCheck(this, InfoBlock);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _possibleConstructorReturn(_this, (_temp = _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(InfoBlock)).call.apply(_getPrototypeOf2, [this].concat(args))), _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "renderContent", function (_ref) {
      var prefixCls = _ref.prefixCls,
          content = _ref.content;

      if (InfoBlock.checkIsValidReactElement(content)) {
        return content;
      }

      if (content == null) {
        return _react.default.createElement("spa", null, "'-'");
      }
    }), _temp));
  }

  _createClass(InfoBlock, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          prefixCls = _this$props.prefixCls,
          title = _this$props.title,
          content = _this$props.content,
          children = _this$props.children,
          onClick = _this$props.onClick,
          className = _this$props.className,
          style = _this$props.style;
      var containerClasses = (0, _classnames2.default)(_defineProperty({}, "".concat(prefixCls), prefixCls), className);
      var isChildrenExist = !!children;
      return _react.default.createElement("div", {
        className: containerClasses,
        style: style,
        onClick: onClick
      }, _react.default.createElement("div", {
        className: "".concat(prefixCls, "__layout__").concat(isChildrenExist ? 'inline' : 'block')
      }, _react.default.createElement("p", {
        className: "".concat(prefixCls, "-title")
      }, InfoBlock.checkIsValidReactElement(title) ? title : _react.default.createElement("span", null, title)), _react.default.createElement("p", {
        className: "".concat(prefixCls, "-content")
      }, // undefined或null默认填充 '-'
      content == null ? '-' : content)), children);
    }
  }]);

  return InfoBlock;
}(_react.Component);

_defineProperty(InfoBlock, "defaultProps", {
  prefixCls: 'creams-info-block',
  title: '标题',
  // titleChild: null,
  content: '-',
  children: null,
  onClick: function onClick(evt) {},
  className: '',
  style: {}
});

_defineProperty(InfoBlock, "propTypes", {
  /** 
   * 前缀名
   * @ignore
   */
  prefixCls: _propTypes.default.string,

  /** 顶部小标题 */
  title: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.node]),

  /** 顶部小标题说明 */
  // titleChild:PropTypes.oneOfType([
  //   PropTypes.string,
  //   PropTypes.node
  // ]),

  /** 底部内容 */
  content: _propTypes.default.any,

  /** 右侧扩展内容 */
  children: _propTypes.default.element,

  /** 点击函数 */
  onClick: _propTypes.default.func,

  /** 外部class覆盖 */
  className: _propTypes.default.string,

  /** 外部行内样式覆盖 */
  style: _propTypes.default.object
});

_defineProperty(InfoBlock, "checkIsValidReactElement", function (target) {
  return _react.default.isValidElement(target);
});

var _default = InfoBlock;
exports.default = _default;