"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames2 = _interopRequireDefault(require("classnames"));

require("./style/index.css");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * @author jianglei
 * @description 图标组件,基于iconfont
 * @example ./docs/icon.md
 * @class Icon
 * @extends {Component}
 */
var Icon =
/*#__PURE__*/
function (_Component) {
  _inherits(Icon, _Component);

  function Icon() {
    _classCallCheck(this, Icon);

    return _possibleConstructorReturn(this, _getPrototypeOf(Icon).apply(this, arguments));
  }

  _createClass(Icon, [{
    key: "render",
    value: function render() {
      var _classnames;

      var _this$props = this.props,
          prefixCls = _this$props.prefixCls,
          type = _this$props.type,
          style = _this$props.style,
          disabled = _this$props.disabled,
          onClick = _this$props.onClick,
          className = _this$props.className,
          feedback = _this$props.feedback;
      var classes = (0, _classnames2.default)((_classnames = {}, _defineProperty(_classnames, "".concat(prefixCls), prefixCls), _defineProperty(_classnames, "icon-".concat(type), type), _defineProperty(_classnames, "".concat(prefixCls, "-disabled-").concat(disabled), disabled), _defineProperty(_classnames, 'creams-icon', feedback === 'default'), _classnames), className);

      var styles = _objectSpread({
        cursor: typeof onClick === 'function' ? 'pointer' : 'initial'
      }, style);

      return _react.default.createElement("i", {
        className: classes,
        style: styles,
        onClick: onClick
      });
    }
  }]);

  return Icon;
}(_react.Component);

_defineProperty(Icon, "defaultProps", {
  prefixCls: 'iconfont',
  type: '',
  style: {},
  className: '',
  disabled: false,
  onClick: null,
  feedback: 'default'
});

_defineProperty(Icon, "propTypes", {
  /** 
   * 前缀名
   * @ignore
   */
  prefixCls: _propTypes.default.string,

  /** 图标类型 */
  type: _propTypes.default.string,

  /** 外部覆盖样式 */
  style: _propTypes.default.object,

  /** 不可点击状态 */
  disabled: _propTypes.default.bool,

  /** 点击事件 */
  onClick: _propTypes.default.oneOfType([_propTypes.default.func, _propTypes.default.instanceOf(null)]),

  /** 外部class覆盖  */
  className: _propTypes.default.string,

  /** 类型 */
  feedback: _propTypes.default.oneOf(['default', 'normal'])
});

var _default = Icon;
exports.default = _default;