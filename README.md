# creams-ui

## 简介

[creams-ui]([https://git.souban.io/Web/creams-ui.git)是基于[ant-design](https://ant.design/docs/react/introduce-cn)进行二次封装的UI库，从而满足公司多方面的业务需求。

## 文件目录

├── es 拷贝的源代码副本  
├── lib  babel编译后的代码文件  
├── node_modules  依赖包  
├── public  用于预览文档的文件，一般用不着  
├── dist  打包输出的文件，用于线上部署  
├── src  源代码文件  
│   └── components  
├── tools  工具函数文件，用于文件拷贝等操作  
│   └── copy-file.mjs  

## 开发

本地Less编译

> 可以使用vscode中Easy-less插件,或者后续改进在脚本中。

本地开发有两种可选方案：

- 依托dist的文档项目预览开发
- 结合npm调试脚本进行开发

npm脚本说明

```js
// 创建全局软链接(当前开发根目录运行)
npm link

// 替换当前开发目录的引用(当前预览根目录运行)
npm link creams-ui
```

## 发布

```js
// 发布前确保源文件和编译文件已经正确处理,如果还没有则进行以下命令
npm run compile
npm run copy

// ⚠️  每次发布需要修改版本号
crnpm publish (发布内部私有仓库)
```

## TODO事项

- 目前组件库跟组件文档耦合依赖,日后需要将两者拆分,减小组件库的体积。

## 底层框架

- [ant-design](https://ant.design/docs/react/introduce-cn)
- [react-styleguidist](https://react-styleguidist.js.org/)